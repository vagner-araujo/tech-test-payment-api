﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [Route("api/venda")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        List<Venda> _vendas = new();

        /// <summary>
        /// Construtor recebendo a injecao de dependencia da lista com as vendas (dados em memoria)
        /// </summary>
        /// <param name="vendas"></param>
        public VendaController(List<Venda> vendas)
        {
            _vendas = vendas;
        }

        /// <summary>
        /// Insere uma nova venda no sistema
        /// </summary>
        /// <remarks>Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento"</remarks>
        /// <response code="200">Venda inserida com sucesso</response>
        /// /// <response code="428">Dados inválido</response>
        /// <response code="500">Oops! Não foi possível inserir a venda</response>
        [HttpPost("create")]
        public IActionResult NovaVenda(NovaVenda novaVenda)
        {
            if (novaVenda.Vendedor.Id <= 0
                || string.IsNullOrEmpty(novaVenda.Vendedor.CPF)
                || string.IsNullOrEmpty(novaVenda.Vendedor.Nome)
                || string.IsNullOrEmpty(novaVenda.Vendedor.Email)
                || string.IsNullOrEmpty(novaVenda.Vendedor.Telefone)
                )
            {
                return StatusCode(428, new { mensagem = $"Todos os dados do vendedor são obrigatórios" });
            }

            if(novaVenda.ItensVenda.Count <=0)
            {
                return StatusCode(428, new { mensagem = $"A inclusão de uma venda deve possuir pelo menos 1 item" });
            }

            Venda venda = new()
            {
                Vendedor = novaVenda.Vendedor,
                Itens = novaVenda.ItensVenda,
                Data = DateTime.Now,
                Status = "Aguardando pagamento",
                IdPedido = _vendas.Count + 1
            };
            _vendas.Add(venda);

            return CreatedAtAction(nameof(Obter), new { id = venda.IdPedido }, venda);

        }

        /// <summary>
        /// Recupera uma venda específica pelo IdPedido
        /// </summary>
        /// <remarks>Buscar venda: Busca pelo Id da venda</remarks>
        /// <response code="200">Venda localizada com sucesso</response>
        /// <response code="400">Venda não localizada</response>
        /// <response code="500">Oops! Algo deu errado</response>
        [HttpGet("{id}")]
        public IActionResult Obter(int id)
        {
            var venda = _vendas.FirstOrDefault(v => v.IdPedido == id);
            if (venda == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(venda);
            }

        }

        /// <summary>
        /// Atualiza o Status da Venda
        /// </summary>
        /// <remarks>
        /// Atualizar venda: Permite que seja atualizado o status da venda
        /// <br/>Status aceitos: 
        /// <ul>
        /// <li>Pagamento aprovado</li>
        /// <li>Enviado para transportadora</li>
        /// <li>Entregue</li>
        /// <li>Cancelada</li>
        /// </ul>
        /// A atualização de status permite somente as seguintes transições:
        /// <ul>
        /// <li>De: Aguardando pagamento Para: Pagamento Aprovado</li>
        /// <li>De: Aguardando pagamento Para: Cancelada</li>
        /// <li>De: Pagamento Aprovado Para: Enviado para Transportadora</li>
        /// <li>De: Pagamento Aprovado Para: Cancelada</li>
        /// <li>De: Enviado para Transportador.Para: Entregue</li>
        /// </ul>
        /// </remarks>
        /// <response code="200">Venda Atualizada</response>
        /// <response code="428">Estado inválido</response>
        /// <response code="500">Oops! Algo deu errado</response>
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, string status)
        {
            List<string> estadosAceitos = new()
            {
                "Pagamento aprovado",
                "Enviado para transportadora",
                "Entregue",
                "Cancelada"
            };
            if (estadosAceitos.FirstOrDefault(x => x.Equals(status, StringComparison.OrdinalIgnoreCase)) == null)
            {
                return StatusCode(428, new { mensagem = $"Não é possivel alterar a venda para o Status '{status}'" });
            }

            Venda venda = _vendas.FirstOrDefault(v => v.IdPedido == id);
            if (venda == null)
            {
                return NotFound();
            }


            if (status.Equals("Pagamento aprovado", StringComparison.OrdinalIgnoreCase)
                && !venda.Status.Equals("Aguardando pagamento", StringComparison.OrdinalIgnoreCase))
            {
                return StatusCode(428, new
                {
                    mensagem = $"Não é possivel alterar a venda para o Status '{status}' " +
                        $"quando a mesma encontra-se no Status '{venda.Status}'"
                });
            }
            else if (status.Equals("Enviado para transportadora", StringComparison.OrdinalIgnoreCase)
                && !venda.Status.Equals("Pagamento aprovado", StringComparison.OrdinalIgnoreCase))
            {
                return StatusCode(428, new
                {
                    mensagem = $"Não é possivel alterar a venda para o Status '{status}' " +
                    $"quando a mesma encontra-se no Status '{venda.Status}'"
                });
            }
            else if (status.Equals("Entregue", StringComparison.OrdinalIgnoreCase)
                && !venda.Status.Equals("Enviado para transportadora", StringComparison.OrdinalIgnoreCase))
            {
                return StatusCode(428, new
                {
                    mensagem = $"Não é possivel alterar a venda para o Status '{status}' " +
                    $"quando a mesma encontra-se no Status '{venda.Status}'"
                });
            }
            else if (status.Equals("Cancelada", StringComparison.OrdinalIgnoreCase)
                && !venda.Status.Equals("Aguardando pagamento", StringComparison.OrdinalIgnoreCase)
                && !venda.Status.Equals("Pagamento Aprovado", StringComparison.OrdinalIgnoreCase))
            {
                return StatusCode(428, new
                {
                    mensagem = $"Não é possivel alterar a venda para o Status '{status}' " +
                    $"quando a mesma encontra-se no Status '{venda.Status}'"
                });
            }
            else
            {
                venda.Status = status;
            }

            int idx = _vendas.FindIndex(v => v.IdPedido == id);
            _vendas[idx].Status = venda.Status;

            return CreatedAtAction(nameof(Obter), new { id = venda.IdPedido }, venda);

        }

    }
}
