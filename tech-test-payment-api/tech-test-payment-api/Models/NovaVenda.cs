﻿namespace tech_test_payment_api.Models
{
    public class NovaVenda
    {
        public Vendedor Vendedor { get; set; }
        public List<ItemVenda> ItensVenda { get; set; }
    }
}
