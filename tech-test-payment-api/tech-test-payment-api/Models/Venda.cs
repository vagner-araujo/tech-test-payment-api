﻿using Newtonsoft.Json;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int IdPedido { get; set; } = -1;
        public Vendedor Vendedor { get; set; } = new Vendedor();
        public DateTime Data { get; set; } = new DateTime();
        public List<ItemVenda> Itens { get; set; } = new List<ItemVenda>();
        public string Status { get; set; } = string.Empty;
    }
}
