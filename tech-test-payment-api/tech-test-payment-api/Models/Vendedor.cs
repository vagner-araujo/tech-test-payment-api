﻿namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        public int Id { get; set; } = -1;
        public string CPF { get; set; } = string.Empty;
        public string Nome { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Telefone { get; set; } = string.Empty;
    }
}
